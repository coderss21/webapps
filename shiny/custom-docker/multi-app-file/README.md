# Multi file shiny app

## Local Dev and testing
### Build it something like this
docker build -t multi-file-app .

### Run it something like this
docker run -it -p 3838:3838 -e SHINYPROXY_USERNAME=test multi-file-app
